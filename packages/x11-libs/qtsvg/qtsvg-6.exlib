# Copyright 2013, 2015, 2017, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtSvg"
DESCRIPTION="Scalable Vector Graphics (SVG) is an XML-based language for describing
two-dimensional vector graphics. Qt provides classes for rendering and displaying
SVG drawings in widgets and on other paint devices."

LICENCES+="
    MIT [[ note = [ xsvg ] ]]
    GPL-2
"
MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
"

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples QT_BUILD_EXAMPLES'
)

qtsvg-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtsvg-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

