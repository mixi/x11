# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.xz ]
require meson [ meson_minimum_version=0.55.0 ]

SUMMARY="An OpenType text shaping engine"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    chafa [[ description = [ Use chafa to produce output for the hb-view cli tool ] ]]
    gobject-introspection
    graphite [[ description = [ Build with Graphite2 font system support ] ]]
    gtk-doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.34.0] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
    build+run:
        dev-libs/glib:2[>=2.19.1]
        dev-libs/icu:=
        media-libs/freetype:2[>=2.4.2]
        x11-libs/cairo[>=1.8.0]
        chafa? ( graphics/chafa )
        graphite? ( x11-libs/graphite2[>=1.2.0] )
    test:
        dev-lang/python:*
"
# NOTE: ragel would be required for a scm version

MESON_SRC_CONFIGURE_PARAMS=(
    -Dcairo=enabled
    -Dfreetype=enabled
    -Dglib=enabled
    -Dgobject=enabled
    -Dicu=enabled
    -Dcoretext=disabled
    -Ddirectwrite=disabled
    -Dgdi=disabled
    # Don't know how useful, but no additional deps and small
    -Dutilities=enabled
    # Experimental
    -Dwasm=disabled

    -Dexperimental_api=false
    -Dicu_builtin=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    chafa
    'gobject-introspection introspection'
    'graphite graphite2'
    'gtk-doc docs'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc doc_tests'
)
MESON_SRC_CONFIGURE_TESTS=( '-Dtests=enabled -Dtests=disabled' )

src_prepare() {
    meson_src_prepare

    # TODO: Fix this properly upstream
    edo sed \
        -e "s:'nm':'$(exhost --tool-prefix)nm':g" \
        -e "s:'c++filt':'$(exhost --tool-prefix)c++filt':g" \
        -i src/check-symbols.py
    edo sed \
        -e "s:'objdump':'$(exhost --tool-prefix)objdump':" \
        -i src/check-static-inits.py
}

