# Copyright 2013-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    SCM_EXTERNAL_REFS="tests/auto/qml/ecmascripttests/test262:"
fi

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtQml, QtQuick aka QtQuick2"
DESCRIPTION="The Qt Quick module is the standard library for writing QML applications. While the
Qt QML module provides the QML engine and language infrastructure, the Qt Quick module provides
all the basic elements necessary for creating user interfaces with QML. It provides a visual canvas and
includes types for creating and animating visual components, receiving user input, creating data
models and views and delayed object instantiation."

LICENCES+="
    BSD-2 [[ note = [ src/3rdparty/masm - JavaScriptCore Macro Assembler ] ]]
    GPL-2
"
MYOPTIONS="examples"

DEPENDENCIES="
    build:
        dev-lang/python:*
    build+run:
        x11-dri/mesa
        x11-libs/qtbase:${SLOT}[>=${PV}][gui][sql]
            [[ note = [ QML and user-specific local storage via SQLite. Might
                        optional -> remove from imports/imports.pro ] ]]
        !x11-libs/qtquickcontrols:5[<5.7.0-rc] [[
            description = [ Parts of qtquickcontrols have been move to qtdeclarative ]
            resolution = uninstall-blocked-after
        ]]
"

qtdeclarative_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtdeclarative_src_compile() {
    # Ugly and probably fragile hack to avoid qmake being utterly stupid
    # and pulling in the old, installed qtdeclarative.
    if has_version 'x11-libs/qtdeclarative[<5.10]' ; then
        # The makefile isn't created at the start of this phase, so we
        # need to call qmake manually to create it and fix it below.
        edo pushd src/qmltest
        edo /usr/$(exhost --target)/lib/qt5/bin/qmake -o Makefile \
            QTDIR="/usr/$(exhost --target)/lib" \
            QMAKE="/usr/$(exhost --target)/lib/qt5/bin/qmake" \
            QMAKE_CFLAGS_RELEASE="${CFLAGS}" \
            QMAKE_CXXFLAGS_RELEASE="${CXXFLAGS}" \
            QMAKE_LFLAGS_RELEASE="${LDFLAGS}" \
            QMAKE_CFLAGS_DEBUG="${CFLAGS}" \
            QMAKE_CXXFLAGS_DEBUG="${CXXFLAGS}" \
            QMAKE_LFLAGS_DEBUG="${LDFLAGS}" \
            CONFIG+=nostrip
            edo sed -e "s:\(= \$(SUBLIBS) \):\1-L${WORK}/lib -lQt5Qml -lQt5Quick -L/usr/$(exhost --target)/lib :" \
                   -i Makefile
        edo popd
    fi

    qt_src_compile
}

qtdeclarative_src_install() {
    qt_src_install

    # install symlinks for some binaries to /usr/host/bin
    local host=$(exhost --target)
    local binaries=( qmlimportscanner qmllint qmlmin qmlplugindump qmlprofiler qmlscene qmltestrunner )

    edo mkdir "${IMAGE}"/usr/${host}/bin
    for i in ${binaries[@]} ; do
        [[ -e "${IMAGE}"/usr/${host}/lib/qt5/bin/${i} ]] || die "/usr/${host}/lib/qt5/bin/${i} does not exist in ${IMAGE}"
        dosym /usr/${host}/lib/qt5/bin/${i} /usr/${host}/bin/${i}-qt5
    done
}

