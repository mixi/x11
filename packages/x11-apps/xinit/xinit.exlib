# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg [ suffix=tar.xz ]

export_exlib_phases src_install

SUMMARY="Various utilities to make it easier to use X"

LICENCES="X11"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        x11-proto/xorgproto
        x11-utils/util-macros[>=1.19.0-r1] [[
            note = [ Introduced a patch for XORG_PROG_RAWCPP to avoid the use of unprefixed cpp ]
        ]]
    build+run:
        x11-libs/libX11
    run:
        x11-apps/xauth
    suggestion:
        (
            x11-apps/xclock
            x11-apps/xmodmap
            x11-apps/xrdb
            x11-apps/xterm
            x11-wm/twm
        ) [[ description = [ Used by default xinitrc ] ]]
"

# NOTE(compnerd) note the triple quoting on the parameters.  The first quoting is for the shell
# invoking the parameters.  The second level of quoting (single quoted) is for the parameter to be
# passed to configure to contain the third level of quotes which are to be embedded into the C
# preprocessed shell script.
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-xrdb="'\"/usr/host/bin/xrdb\"'"
    --with-xmodmap="'\"/usr/host/bin/xmodmap\"'"
    --with-twm="'\"/usr/host/bin/twm\"'"
    --with-xclock="'\"/usr/host/bin/xclock\"'"
    --with-xterm="'\"/usr/host/bin/xterm\"'"
    --with-xserver="'\"/usr/$(exhost --target)/bin/X\"'"
    --with-xinit="'\"/usr/$(exhost --target)/bin/xinit\"'"
    --without-launchd
    MCOOKIE="\\\"/usr/$(exhost --target)/bin/mcookie\\\""
)

xinit_src_install() {
    default

    edo sed -e "s,/bin/sh,/usr/$(exhost --target)/bin/sh,"  \
            -i "${IMAGE}/usr/$(exhost --target)/bin/startx"

    insinto /etc/X11/xinit
    doins "${FILES}"/xserverrc

}

