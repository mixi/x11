# Copyright 2009-2014 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.xz ] \
    cmake \
    python [ blacklist=none multibuild=false with_opt=true ] \
    freedesktop-desktop \
    freedesktop-mime \
    gtk-icon-cache

SUMMARY="FontForge - An Outline Font Editor"
DESCRIPTION="
FontForge -- An outline font editor that lets you create your own postscript, truetype, opentype,
cid-keyed, multi-master, cff, svg and bitmap (bdf, FON, NFNT) fonts, or edit existing ones. Also
lets you convert one format to another. FontForge has support for many macintosh font formats.
FontForge's user interface has been localized for: (English), Russian, Japanese, French, Italian,
Spanish, Vietnamese, Greek, Simplified & Traditional Chinese, German, Polish, Ukrainian.
"
HOMEPAGE+=" https://fontforge.github.io"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/en-US/documentation [[ lang = en ]]"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gui [[ description = [ Additionally build the GDK-based graphical user interface ] ]]
    tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# Require inet access
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.25]
    build+run:
        dev-libs/glib:2[>=2.6]
        dev-libs/libffi:=
        dev-libs/libxml2:2.0
        media-libs/freetype:2[>=2.3.7]
        media-libs/giflib:=
        media-libs/libpng:=[>=1.2]
        sys-libs/readline:=
        sys-libs/zlib
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        gui? (
            x11-libs/cairo
            x11-libs/gtk+:3[>=3.10]
            x11-libs/pango
        )
        tiff? ( media-libs/tiff:= )
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-Mark-translations-with-invalid-directives-as-fuzzy.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DENABLE_CODE_COVERAGE:BOOL=FALSE
    -DENABLE_DEBUG_RAW_POINTS:BOOL=FALSE
    -DENABLE_DOCS:BOOL=FALSE
    -DENABLE_FONTFORGE_EXTRAS:BOOL=FALSE
    -DENABLE_X11:BOOL=FALSE
    -DENABLE_LIBGIF:BOOL=TRUE
    -DENABLE_LIBJPEG:BOOL=TRUE
    -DENABLE_LIBPNG:BOOL=TRUE
    -DENABLE_LIBREADLINE:BOOL=TRUE
    -DENABLE_LIBSPIRO:BOOL=FALSE
    -DENABLE_MAINTAINER_TOOLS:BOOL=FALSE
    -DENABLE_NATIVE_SCRIPTING:BOOL=TRUE
    -DENABLE_SANITIZER:ENUM=none
    -DENABLE_TILE_PATH:BOOL=FALSE
    -DENABLE_WOFF2:BOOL=FALSE
    -DENABLE_WRITE_PFM:BOOL=FALSE
    -DPython3_EXECUTABLE:PATH=${PYTHON}
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'gui GUI'
    'python PYTHON_EXTENSION'
    'python PYTHON_SCRIPTING'
    'tiff LIBTIFF'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

